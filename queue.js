let collection = [];

// Write the queue functions below.
print = () => {
    return collection;
};

size = () => {
    return collection.length;
};

enqueue = (e) => {
    collection[size()] = e;
    return collection;
};

dequeue = () => {
    collection.splice(0, 1);
    return collection;
};

isEmpty = () => {
    return collection.length == 0;
};

front = () => {
    return collection[0];
};



module.exports = {
    enqueue,
    dequeue,
    print,
    front,
    size,
    isEmpty
};